import styled from 'styled-components';
import { Link } from 'react-router-dom';

export type StylingProps = {
    margin?: '25vh auto auto';
    marginTop?: '30px' | 'none';
    fontSize?: '55px' | '35px' | '20px';
    color?: 'transparent' | 'black';
    textShadow?: '0 0 1.6px white';
    padding?: '20px' | '10px';
    paddingLeft?: '25px';
    fontWeight?: 'bold';
    justifySelf?: 'flex-start';
    marginRight?: 'auto';
};

export const StyledLink = styled(Link)<StylingProps>`
    display: flex;
    justify-self: ${props => props.justifySelf};
    text-decoration: none;
    margin-right: ${props => props.marginRight};
`;

export const Logo = styled.div<StylingProps>`
    display: flex;
    margin: ${props => props.margin};
    justify-content: center;

    text-decoration: none;

    color: #0c1f2f;
    text-align: center;
    font-size: ${props => props.fontSize};
    text-shadow: ${props => props.textShadow};
    font-weight: bold;

    cursor: pointer;
`;

import React from 'react';

import { Logo } from '../../../gobalStyles/globalStyles';

const MainProfile = () => {
    return (
        <Logo fontSize='55px' color='black'>
            ProfileContent
        </Logo>
    );
};

export default MainProfile;

import styled from 'styled-components';

import { StylingProps } from '../../gobalStyles/globalStyles';

export const FeedWrapper = styled.div`
    display: flex;
    flex-direction: column;
    gap: 10px;
`;

export const Post = styled.div<StylingProps>`
    display: flex;
    flex-direction: column;
    height: 800px;
    width: 650px;
    align-items: center;

    margin: auto;
    border: 1px grey solid;

    background: #ded8d8;
    font-size: 30px;
`;

export const StyledImage = styled.img`
    display: flex;
    border: 1px solid grey;
    background-repeat: no-repeat;
    background-position: center;
    background-attachment: fixed;
    background-size: cover;
`;

export const StyledPostElement = styled.div<StylingProps>`
    display: flex;
    align-self: flex-start;

    color: #0c1f2f;

    padding: ${props => props.padding};
    padding-left: ${props => props.paddingLeft};
    font-size: ${props => props.fontSize};
    font-weight: ${props => props.fontWeight};
`;

import { Post } from '../../../../store/feed/feed.types';

export interface MockedPostProps {
    post: Post;
}

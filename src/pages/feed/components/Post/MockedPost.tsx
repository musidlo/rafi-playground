import React from 'react';

import { Post, StyledImage, StyledPostElement } from '../../Feed.styles';
import { MockedPostProps } from './MockedPost.types';

const MockedPost = (props: MockedPostProps) => {
    const { post } = props;

    return (
        <Post>
            <StyledPostElement padding='20px' paddingLeft='25px' fontWeight='bold'>
                {post.username}
            </StyledPostElement>
            <StyledImage src={post.link} width='600' height='600' />
            <StyledPostElement padding='10px' paddingLeft='25px' fontWeight='bold'>
                {post.username}
            </StyledPostElement>
            <StyledPostElement fontSize='20px' paddingLeft='25px'>
                {post.desc}
            </StyledPostElement>
        </Post>
    );
};

export default MockedPost;

import React from 'react';
import { useSelector } from 'react-redux';

import { FeedWrapper } from './Feed.styles';
import MockedPost from './components/Post/MockedPost';
import { RootState } from '../../store';

const Feed = () => {
    const posts = useSelector((state: RootState) => state.feed.posts);

    return (
        <FeedWrapper>
            {posts.map((currentMapped, index) => (
                <MockedPost post={currentMapped} key={index} />
            ))}
        </FeedWrapper>
    );
};
export default Feed;

import React, { ChangeEvent, useState } from 'react';
import { useSelector } from 'react-redux';

import { AddPostFormStyle } from './AddPostForm.styles';
import { AddPostFormProps } from './AddPostForm.types';
import { RootState } from '../../../../store';

//Button do globala
import { Button } from '../../../login/LoginPage.styles';
import { StyledLink } from '../../../../gobalStyles/globalStyles';

const AddPostForm = (props: AddPostFormProps) => {
    const user = useSelector((state: RootState) => state.auth.user);

    const [link, setLink] = useState('');
    const [desc, setDesc] = useState('');

    const handleLinkChange = (event: ChangeEvent<HTMLInputElement>) => setLink(event.target.value);
    const handleDescChange = (event: ChangeEvent<HTMLInputElement>) => setDesc(event.target.value);

    const handleAddPostClick = () => {
        props.handleAddPostClick({ link, desc, username: user?.username });
        setLink('');
        setDesc('');
    };

    return (
        <AddPostFormStyle>
            <div>Link do zdjęcia:</div>
            <input placeholder='Link' type='text' value={link} onChange={handleLinkChange} />
            <div>Dodaj opis:</div>
            <input placeholder='Opis zdjęcia' type='text' value={desc} onChange={handleDescChange} />
            <div />
            <StyledLink to='/'>
                <Button onClick={handleAddPostClick}>Dodaj post</Button>
            </StyledLink>
        </AddPostFormStyle>
    );
};

export default AddPostForm;

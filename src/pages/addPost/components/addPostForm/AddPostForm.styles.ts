import styled from 'styled-components';

export const AddPostFormStyle = styled.div`
    display: flex;
    width: 500px;
    flex-direction: column;
    align-items: center;
    gap: 15px;

    padding: 5vh;
    margin: auto;
    border-radius: 10px;

    background: linear-gradient(#a7b4ea, #a99da9);
    color: #0c1f2f;
    font-family: sans-serif;
    font-size: 20px;
    font-weight: bold;

    input:focus {
        outline: none;
    }
`;

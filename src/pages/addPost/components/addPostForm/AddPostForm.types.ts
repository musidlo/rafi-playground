import { Post } from '../../../../store/feed/feed.types';

export interface AddPostFormProps {
    handleAddPostClick: (post: Post) => void;
}

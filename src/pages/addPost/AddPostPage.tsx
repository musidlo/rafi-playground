import React from 'react';
import { useDispatch } from 'react-redux';

import AddPostForm from './components/addPostForm/AddPostForm';
import { addPost } from '../../store/feed/feed.actions';
import { Post } from '../../store/feed/feed.types';

const AddPostPage = () => {
    const dispatch = useDispatch();
    const handleAddPostClick = (post: Post) => dispatch(addPost(post));

    return <AddPostForm {...{ handleAddPostClick }} />;
};

export default AddPostPage;

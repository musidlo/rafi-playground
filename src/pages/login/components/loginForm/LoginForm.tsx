import React, { ChangeEvent, useState } from 'react';

import { LoginWrapper, Button } from '../../LoginPage.styles';
import { LoginFormProps } from './LoginForm.types';

const LoginForm = (props: LoginFormProps) => {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');

    const handleLoginChange = (event: ChangeEvent<HTMLInputElement>) => setLogin(event.target.value);
    const handlePasswordChange = (event: ChangeEvent<HTMLInputElement>) => setPassword(event.target.value);

    const handleLoginClick = () => {
        props.handleLoginClick(login, password);
        setLogin('');
        setPassword('');
    };

    return (
        <LoginWrapper>
            <div>Login</div>
            <input placeholder={'Login'} type='text' value={login} onChange={handleLoginChange} />
            <div>Hasło</div>
            <input placeholder={'Hasło'} type='password' value={password} onChange={handlePasswordChange} />
            <Button marginTop='30px' onClick={handleLoginClick}>
                Zaloguj się
            </Button>
        </LoginWrapper>
    );
};

export default LoginForm;

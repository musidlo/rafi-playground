export interface LoginFormProps {
    handleLoginClick: (login: string, password: string) => void;
}

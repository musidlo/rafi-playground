import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';

import { Logo } from '../../gobalStyles/globalStyles';
import LoginForm from './components/loginForm/LoginForm';
import { login } from '../../store/auth/auth.actions';

interface LocationState {
    from: {
        pathname: string;
    };
}

const LoginPage = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation<LocationState>();

    const { from } = location.state || { from: { pathname: '/' } };

    const handleLoginClick = (username: string, password: string) => {
        dispatch(login({ username }));
        history.replace(from);
    };

    return (
        <>
            <Logo margin='25vh auto auto' fontSize='55px' color='black'>
                Instagram
            </Logo>
            <LoginForm {...{ handleLoginClick }} />
        </>
    );
};

export default LoginPage;

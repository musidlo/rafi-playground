import styled from 'styled-components';

import { StylingProps } from '../../gobalStyles/globalStyles';

export const LoginWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 5px;

    margin-top: 20px;

    color: #0c1f2f;
    font-family: sans-serif;
    font-size: 20px;
    font-weight: bold;

    input:focus {
        outline: none;
    }
`;

export const Button = styled.button<StylingProps>`
    display: flex;
    justify-content: center;
    align-items: center;

    width: 150px;
    height: 30px;

    margin-top: ${props => props.marginTop};
    border: none;
    border-radius: 20px;

    background: #a7b4ea;
    box-shadow: 0 0 10px black;
    color: #0c1f2f;
    cursor: pointer;
    text-decoration: none;
    font-weight: bold;
    outline: none;
    transition: 1s;

    &:hover {
        transition: 1s;
        background: linear-gradient(#a7b4ea, #a99da9);
    }
`;

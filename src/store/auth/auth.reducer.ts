import { AuthState, LOGIN, LOGOUT, AuthActionTypes } from './auth.types';

const initialState: AuthState = {
    user: undefined,
};

export const authReducer = (state = initialState, action: AuthActionTypes): AuthState => {
    switch (action.type) {
        case LOGIN:
            return { ...state, user: action.payload };
        case LOGOUT:
            return { ...state, user: initialState.user };
        default:
            return state;
    }
};

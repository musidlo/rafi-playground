import { AuthActionTypes, LOGIN, LOGOUT, User } from './auth.types';

export const login = (user: User): AuthActionTypes => ({
    type: LOGIN,
    payload: user,
});

export const logout = (): AuthActionTypes => ({
    type: LOGOUT,
});

export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';

export interface User {
    username: string;
}

export interface AuthState {
    user?: User;
}

export interface LoginAction {
    type: typeof LOGIN;
    payload: User;
}

export interface LogoutAction {
    type: typeof LOGOUT;
}

export type AuthActionTypes = LoginAction | LogoutAction;

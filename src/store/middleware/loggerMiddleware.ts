import { Action, Dispatch, MiddlewareAPI } from 'redux';

const logger = (store: MiddlewareAPI) => (next: Dispatch) => (action: Action) => {
    console.group(action.type);
    console.info('dispatching', action);
    let result = next(action);
    console.log('next state', store.getState());
    console.groupEnd();
    return result;
};

export default logger;

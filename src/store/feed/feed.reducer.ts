import { ADD_POST, FeedState, FeedTypes } from './feed.types';

const initialState: FeedState = {
    posts: [],
};

export const feedReducer = (state = initialState, action: FeedTypes): FeedState => {
    switch (action.type) {
        case ADD_POST:
            return { ...state, posts: [action.payload, ...state.posts] };
        default:
            return state;
    }
};

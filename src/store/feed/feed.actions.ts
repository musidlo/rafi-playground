import { ADD_POST, FeedTypes, Post } from './feed.types';

export const addPost = (post: Post): FeedTypes => ({
    type: ADD_POST,
    payload: post,
});

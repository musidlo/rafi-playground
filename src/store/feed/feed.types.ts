export const ADD_POST = 'ADD_POST';

export interface Post {
    link: string;
    desc: string;
    username: string | undefined;
}

export interface FeedState {
    posts: Post[];
}

export interface AddPostAction {
    type: typeof ADD_POST;
    payload: Post;
}

export type FeedTypes = AddPostAction;

import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { authReducer } from './auth/auth.reducer';
import { feedReducer } from './feed/feed.reducer';
import loggerMiddleware from './middleware/loggerMiddleware';

export const rootReducer = combineReducers({
    auth: authReducer,
    feed: feedReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export const configureStore = (initialState?: RootState) => {
    const middlewares = [loggerMiddleware, thunkMiddleware];
    const middlewareEnhancer = applyMiddleware(...middlewares);

    const enhancers = [middlewareEnhancer];
    const composedEnhancers = composeWithDevTools(...enhancers);

    return createStore(rootReducer, initialState, composedEnhancers);
};

//tego nie powinno być w projekcie Reactowym tylko w paczce wspoldzielonej pomiedzy frontem i backiem
export interface Person {
    name: string;
    surname: string;
    age: number;
}

export interface ExampleComponentProps {
    people: Person[];
}

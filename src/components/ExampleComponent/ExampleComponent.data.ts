import { Person } from './ExampleComponent.types';

export const mockedPeople: Person[] = [
    {
        name: 'Mikolaj',
        surname: 'Musidlowski',
        age: 24,
    },
    {
        name: 'Radek',
        surname: 'Baran',
        age: 23,
    },
];

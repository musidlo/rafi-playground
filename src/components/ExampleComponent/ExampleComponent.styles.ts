import styled from 'styled-components';

export const PersonWrapper = styled.div`
    width: 80%;

    margin: 0 auto;
`;

export const PersonItem = styled.div`
    display: flex;
    justify-content: space-between;

    width: 100%;

    margin: 20px 0;
    padding: 20px;

    background: white;
`;

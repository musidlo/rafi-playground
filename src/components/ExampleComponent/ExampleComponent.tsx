import React from 'react';

import { ExampleComponentProps } from './ExampleComponent.types';
import { PersonWrapper, PersonItem } from './ExampleComponent.styles';

const ExampleComponent = (props: ExampleComponentProps) => {
    return (
        <PersonWrapper>
            {props.people.map((person, index) => (
                <PersonItem key={index}>
                    <p>{person.name}</p>
                    <p>{person.surname}</p>
                    <p>{person.age}</p>
                </PersonItem>
            ))}
        </PersonWrapper>
    );
};

export default ExampleComponent;

import styled from 'styled-components';

export const NavBarWrapper = styled.header`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    gap: 10px;
    box-shadow: 0 0 25px black;

    margin: 20px;
    padding: 10px 30px 10px 30px;
    border-radius: 40px;

    background: linear-gradient(#a7b4ea, #a99da9);
    color: orange;
    font-size: 60px;
`;

import React from 'react';
import { useDispatch } from 'react-redux';
import { RootState } from '../../store';
import { useSelector } from 'react-redux';

import { NavBarWrapper } from './NavBar.styles';
import { Logo } from '../../gobalStyles/globalStyles';
import { logout } from '../../store/auth/auth.actions';
import { StyledLink } from '../../gobalStyles/globalStyles';

const NavBar = () => {
    const dispatch = useDispatch();

    const user = useSelector((state: RootState) => state.auth.user);

    const handleLogoutClick = () => dispatch(logout());

    return (
        <NavBarWrapper>
            <StyledLink marginRight='auto' justifySelf='flex-start' to='/'>
                <Logo fontSize='35px' color='black'>
                    Instagram
                </Logo>
            </StyledLink>
            <StyledLink to='addpost'>
                <Logo fontSize='20px' color='black'>
                    Dodaj post
                </Logo>
            </StyledLink>
            <StyledLink to='mainprofile'>
                <Logo fontSize='20px' color='black'>
                    {user?.username}
                </Logo>
            </StyledLink>
            <StyledLink to='settings'>
                <Logo fontSize='20px' color='black'>
                    Ustawienia
                </Logo>
            </StyledLink>
            <Logo fontSize='20px' color='black' onClick={handleLogoutClick}>
                Wyloguj się
            </Logo>
        </NavBarWrapper>
    );
};

export default NavBar;

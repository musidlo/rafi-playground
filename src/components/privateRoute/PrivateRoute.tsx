import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { RouteProps } from 'react-router';

import { RootState } from '../../store';

const PrivateRoute = ({ children, ...rest }: RouteProps) => {
    const user = useSelector((state: RootState) => state.auth.user);

    return (
        <Route
            {...rest}
            render={({ location }) =>
                user ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: '/login',
                            state: { from: location },
                        }}
                    />
                )
            }
        />
    );
};

export default PrivateRoute;

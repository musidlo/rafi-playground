import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { RootState } from './store';
import { useSelector } from 'react-redux';

import LoginPage from './pages/login/LoginPage';
import Feed from './pages/feed/Feed';
import AddPostPage from './pages/addPost/AddPostPage';
import Settings from './pages/settings/components/Settings';
import MainProfile from './pages/mainProfile/components/MainProfile';
import NavBar from './components/navBar/NavBar';
import PrivateRoute from './components/privateRoute/PrivateRoute';

const App = () => {
    const user = useSelector((state: RootState) => state.auth.user);

    return (
        <Router>
            {user && <NavBar />}
            <Switch>
                <Route path='/login'>
                    <LoginPage />
                </Route>
                <PrivateRoute path='/' exact>
                    <Feed />
                </PrivateRoute>
                <PrivateRoute path='/settings' exact>
                    <Settings />
                </PrivateRoute>
                <PrivateRoute path='/mainprofile' exact>
                    <MainProfile />
                </PrivateRoute>
                <PrivateRoute path='/addpost' exact>
                    <AddPostPage />
                </PrivateRoute>
            </Switch>
        </Router>
    );
};

export default App;
